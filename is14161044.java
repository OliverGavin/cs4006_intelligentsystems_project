/*
Bash helpers:

  Get count of each cost:
    javac is14161044.java && java is14161044 && cat AI17.txt | grep cost | cut -d':' -f4 | sort | uniq -c


*/

// TODO: Take probabilities, validate, tidy, check if more output needed, quick exit if fitness is 0?

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.io.PrintWriter;
import java.io.IOException;

import java.util.logging.Logger;

public class is14161044 {

  public static void main(String[] args) {

    // int generations = getUserInput("Number of generations: ", false);      // G
    // int population = getUserInput("Population size: ", false);             // P
    // int students = getUserInput("Number of students: ", false);            // S
    // int modules = getUserInput("Total number of modules: ", true);         // M
    // int choices = getUserInput("Number of modules in a course: ", false);  // C

    // int generations = 20;
    // int population = 10;
    // int students = 7;
    // int modules = 14;
    // int choices = 5;

    int generations = 100;
    int population = 15;
    int students = 15;
    int modules = 20;
    int choices = 5;

    if (choices >= modules) {
      System.out.println("Number of modules in a course (C) must be less than the total number of modules (M).");
      System.exit(1);
    }

    TimetableOptimiser t = new TimetableOptimiser(generations, population, students, modules, choices);
    t.start();

    // Print to file
    try ( PrintWriter writer = new PrintWriter("AI17.txt", "UTF-8") ) {

      // Print student schedules
      int i = 1;
      for (HashSet<Integer> schedule : t.getStudentSchedule()) {
        String line = String.format("Student %d:", i++);
        for (int module : schedule) {
          line += String.format(" m%d", module).toUpperCase();
        }
        writer.println(line);
      }

      // Print orderings
      ArrayList<Ordering> orderings = t.getCurrentGeneration();
      printOrderings(orderings, writer);

    } catch (IOException e) {

    }

  }

  static void printOrderings(ArrayList<Ordering> orderings, PrintWriter writer) {
    for (int i = 0; i < orderings.size(); i++) {
      Ordering ordering = orderings.get(i);
      String line1 = String.format("Ord%d:", i+1);
      String line2 = "     ";

      int j = 0;
      for (int [] pair : ordering.getValues()) {
        line1 += String.format("\tm%d", pair[0]);
        line2 += String.format("\tm%d", pair[1]);
        j++;
      }

      line1 += String.format(":\tcost: %d", ordering.getFitness());
      writer.println();
      writer.println(line1);
      writer.println(line2);
    }
  }

  static int getUserInput(String msg, boolean even) {
    Scanner reader = new Scanner(System.in);
    boolean valid = false;
    int x = 0;

    while (!valid) {
      System.out.println(msg);
      try {
        x = reader.nextInt();
        if (x < 0)
          throw new java.util.InputMismatchException();
        if (even && x%2 != 0)
          System.out.println("An even integer was expected. Try again. CTRL+C to exit.");
        else
          valid = true;
      } catch (java.util.InputMismatchException e) {
        System.out.println("A positive integer was expected. Try again. CTRL+C to exit.");
      }
    }

    return x;
  }

}

class Ordering {
  private int[][] values;
  private int fitness;

  public Ordering(int[][] values, int fitness) {
    this.values = values;
    this.fitness = fitness;
  }

  public Ordering(Ordering source) {
    this.fitness = source.fitness;
    int[][] sourceValues = source.getValues();
    this.values = new int[sourceValues.length][sourceValues[0].length];
    for (int i = 0; i < this.values.length; i++)
         this.values[i] = Arrays.copyOf(sourceValues[i], sourceValues[i].length);
  }

  public int[][] getValues() {
    return this.values;
  }

  public void setValues(int[][] values) {
    this.values = values;
  }

  public int getFitness() {
    return this.fitness;
  }

  public void setFitness(int fitness) {
    this.fitness = fitness;
  }

}

class TimetableOptimiser {
  private int numGenerations;
  private int populationSize;
  private int numStudents;
  private int numModulesInTotal;
  private int numModulesInCourse;
  private int numExamDays;

  private double probReproduction = 0.8;  // TODO: take as input and validate total
  private double probCrossover = 0.15;
  private double probMutation = 0.05;

  private ArrayList<Integer> modules = new ArrayList<Integer>();
  private ArrayList<HashSet<Integer>> studentSchedule = new ArrayList<HashSet<Integer>>();  // S x C -> Ordering doesn't matter, but a set will give us O(1) search
  private ArrayList<Ordering> currentGeneration = new ArrayList<Ordering>();
  private ArrayList<Ordering> nextGeneration = new ArrayList<Ordering>();

  private static Logger logger = Logger.getLogger("is14161044.TimetableOptimiser");

  public TimetableOptimiser(int generations, int population, int students, int modules, int choices) {
    this.numGenerations = generations;
    this.populationSize = population;
    this.numStudents = students;
    this.numModulesInTotal = modules;
    this.numModulesInCourse = choices;
    this.numExamDays = modules/2;
  }

  public void start() {
    generateModules();
    generateStudentSchedule();
    logger.info("Generation: " + 0);
    generateCurrentGeneration();
    shuffleCurrentGeneration();

    for (int g = 1; g < this.numGenerations; g++) {
      // For each generation
      logger.info("Generation: " + g);

      // Reset previous
      nextGeneration = new ArrayList<Ordering>();

      // Apply selection on current
      applySelection();


      int i = 0;  // Index of ordering
      while (i < this.populationSize) {

        // Choose and apply GA techniques
        double p = Math.random();
        double probCumulative = 0.0;

        if (p <= (probCumulative+=probReproduction)) {
          logger.info("Reproduction @Pr(" + p + "): ");
          applyReproduction();
          i++;
        }
        else if (this.currentGeneration.size() >= 2 && p <= (probCumulative+=probCrossover)) {
          logger.info("Crossover @Pr(" + p + "): ");
          applyCrossover();
          i+=2;
        }
        else if (p <= (probCumulative+=probMutation)) {
          logger.info("Mutation @Pr(" + p + "): ");
          applyMutation();
          i++;
        }

      }

      currentGeneration = nextGeneration;

    }

    // Choose the best
    Ordering ordering = this.currentGeneration.stream().min(Comparator.comparingInt(Ordering::getFitness)).get();
    String line1 = "Best Ordering:\n\n";
    String line2 = "";

    int j = 0;
    for (int [] pair : ordering.getValues()) {
      line1 += String.format("\tm%d", pair[0]);
      line2 += String.format("\tm%d", pair[1]);
      j++;
    }

    line1 += String.format(":\tcost: %d", ordering.getFitness());
    System.out.println();
    System.out.println(line1);
    System.out.println(line2);

  }

  public Ordering popRandomOrdering() {
    int x = new Random().nextInt(this.currentGeneration.size());
    Ordering popped = this.currentGeneration.get(x);
    this.currentGeneration.remove(x);
    return popped;
  }

  public void applyReproduction() {
      Ordering ordering = popRandomOrdering();
      this.nextGeneration.add(ordering);
  }

  public void applyCrossover() {
  // try ( PrintWriter writer = new PrintWriter("debug.txt", "UTF-8") ) {

    Ordering ordering1 = popRandomOrdering();
    Ordering ordering2 = popRandomOrdering();

  // is14161044.printOrderings(new ArrayList<Ordering>(Arrays.asList(ordering1,ordering2)), writer);

    // Get a cut point somewhere between the first and last element (at least one value on each side of cut)
    int cp = new Random().nextInt(this.numExamDays*2 - 2) + 1;
    logger.info("Crossover cutting point: " + cp);

    int [][] values1 = ordering1.getValues();
    int [][] values2 = ordering2.getValues();

    Set<Integer> values1UpToCp = new HashSet<>();
    Set<Integer> values2UpToCp = new HashSet<>();

    // Swap values in place up to the cut point
    for (int i = 0; i < cp; i++) {
      // Simulates a flattened array
      int col = i % this.numExamDays;
      int row = i / this.numExamDays;

      // swap
      int temp = values1[col][row];
      values1[col][row] = values2[col][row];
      values2[col][row] = temp;

      // store for duplicate checking
      values1UpToCp.add(values1[col][row]);
      values2UpToCp.add(values2[col][row]);

    }

  // writer.println("\nCrossover with dupes:");
  // is14161044.printOrderings(new ArrayList<Ordering>(Arrays.asList(ordering1,ordering2)), writer);

    // Since duplicated numbers will be on either side of the crossover point (but never on the same side),
    // the symetric difference of either side of both orderings will give us
    // the duplicate numbers.
    // This can be done because we know that each ordering will always contain the complete set of modules.
    Set<Integer> duplicates = symetricDifference(values1UpToCp, values2UpToCp);
    // We do not need to worry about missing numbers as they are resolved by fixing duplicates.
    // Since a duplicate pair will always reside in only one ordering, we can store all the
    // duplicates together. We can then iterate through both orderings checking each numbers
    // to see if it is in the list of duplicates. A duplicate found in ordering 1 can be swapped
    // with a duplicate found in ordering 2.

    String line = "";
    for (int x : duplicates)
      line += "  " + x;
    System.out.println(line);

    int a = cp, b = cp; // index of ordering 1 and 2
    boolean a_wait = false, b_wait = false; // Flag to wait until duplicate found in other ordering
    // By starting from cp, all swaps will done on the other side of the cut point
    // This means that if e.g. the left side happened to be the same in both orderings
    // the result will not be the same as the input
    while (duplicates.size() > 0) {

      if (!a_wait) {
      int a_col = a % this.numExamDays;
      int a_row = a / this.numExamDays;
        if (duplicates.contains(values1[a_col][a_row])) {
          a_wait = true;
        } else {
          a++;
        }
      }

      if (!b_wait) {
        int b_col = b % this.numExamDays;
        int b_row = b / this.numExamDays;
        if (duplicates.contains(values2[b_col][b_row])) {
          b_wait = true;
        } else {
          b++;
        }
      }

      if (a_wait && b_wait) {
        int a_col = a % this.numExamDays;
        int a_row = a / this.numExamDays;
        int b_col = b % this.numExamDays;
        int b_row = b / this.numExamDays;
        // Both have a duplicate pending, therfore remove and swap
        duplicates.remove(values1[a_col][a_row]);
        duplicates.remove(values2[b_col][b_row]);

        int temp = values1[a_col][a_row];
        values1[a_col][a_row] = values2[b_col][b_row];
        values2[b_col][b_row] = temp;

        a_wait = false;
        a++;
        b_wait = false;
        b++;
      }

    }

    ordering1.setFitness(calcFitnessCost(values1));
    ordering2.setFitness(calcFitnessCost(values2));

    this.nextGeneration.add(ordering1);
    this.nextGeneration.add(ordering2);



  // writer.println();
  // is14161044.printOrderings(new ArrayList<Ordering>(Arrays.asList(ordering1,ordering2)), writer);
  // writer.flush();
  // } catch (IOException e) {
  // }
  // System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
  // System.exit(0);

  }

  private Set<Integer> symetricDifference(Set<Integer> set1, Set<Integer> set2) {
    Set<Integer> symmetricDiff = new HashSet<Integer>(set1);
    symmetricDiff.addAll(set2);
    // symmetricDiff now contains the union

    Set<Integer> tmp = new HashSet<Integer>(set1);
    tmp.retainAll(set2);
    // tmp now contains the intersection

    symmetricDiff.removeAll(tmp);
    // union minus intersection equals symmetric-difference

    return symmetricDiff;
  }

  public void applyMutation() {
    Ordering ordering = popRandomOrdering();
    int [][] values = ordering.getValues();

    int i = new Random().nextInt(values.length);
    int j = i;
    while (j == i) {
      j = new Random().nextInt(values.length);
    }

    int ir = new Random().nextInt(2);
    int jr = new Random().nextInt(2);

    int temp = values[i][ir];
    values[i][ir] = values[j][jr];
    values[j][jr] = temp;

    ordering.setValues(values);
    ordering.setFitness(calcFitnessCost(values));

    this.nextGeneration.add(ordering);
  }

  /*
   *  Generates the required number of modules
   *
   *  In:
   *    numModulesInTotal (int):  Total number of modules
   *  Out:
   *    modules (ArrayList<int>): List of all modules represented as integers
   */
  private void generateModules() {
    for (int i = 1; i <= this.numModulesInTotal; i++) {
      modules.add(i);
    }
  }

  /*
   *  Generates the required number of random student schedules/modules
   *  Random, efficient generation is achieved by shuffling the modules and picking in sequence
   *
   *  In:
   *    numStudents (int):        Total number of students
   *    numModulesInCourse (int): Number of modules a single student may take
   *  Out:
   *    studentSchedule (ArrayList<HashSet<Integer>>): student schedules/modules represented as integers
   */
  private void generateStudentSchedule() {
    for (int i = 0; i < this.numStudents; i++) {
      Collections.shuffle(modules);   // Random order without having to check if the number was already generated
      HashSet<Integer> schedule = new HashSet<Integer>();   // Since order doesn't matter
                                                            // O(1) look up will be benefitial when calculating the fitness
      for (int j = 0; j < this.numModulesInCourse; j++) {
        schedule.add( modules.get(j) );
      }
      studentSchedule.add(schedule);
    }
  }

  /* NOTE: The alternative sorted method below is being used instead, keeping for comparisson in part 2 of the project
   * Generates a random ordering (exam schedule) of pairs
   * Random, efficient generation is achieved by shuffling the modules and picking in sequence
   *
   * In:
   *  numExamDays (int):    Number of days exams will run (Total number of modules divided by 2 per day)
   *  modules (ArrayList<Integer>): List of all modules to be scheduled
   *
   * Out:
   *  int[][]:  An ordering (A random exam schedule)
   *
   */
  private int[][] generateOrdering() {
    int [][] ordering = new int[this.numExamDays][2];
    Collections.shuffle(modules);
    for (int i = 0; i < ordering.length; i++) {
      ordering[i][0] = modules.get(i);                          // pick from start
      ordering[i][1] = modules.get(modules.size() - (1 + i));   // pick from end
    }
    return ordering;
  }

  /*
   * Generates a random sorted ordering (exam schedule) of pairs
   * The ordering is sorted on insertion in ascending order for both the pair in the schedule and the schedule itself
   *
   * Example:
   *    m5  m1  m6   =>   m1  m3  m4
   *    m3  m2  m4        m2  m5  m6
   *
   * The reason for sorting the order is that searching to see if an ordering is unique is more efficient.
   * Given that 2 exams run per day, m1 and m2, order doesn't matter and [1,2]==[2,1]. Sorting allows us
   * to avoid checking both against every single ordering.
   * When checking that the set of pairs generated are unique (i.e. non duplicated ordering), by sorting,
   * we can stop comparing on the first difference found as we are guaranteed they are different.
   *
   * Random, efficient generation is achieved by shuffling the modules and picking in sequence
   *
   * In:
   *  numExamDays (int):    Number of days exams will run (Total number of modules divided by 2 per day)
   *  modules (ArrayList<Integer>): List of all modules to be scheduled
   *
   * Out:
   *  int[][]:  An ordering (A random exam schedule)
   *
   * If sorting is a problem for the GA in part 2 of the project, it can just be reshuffled. I will compare performance.
   */
  private int[][] generateSortedOrdering() {
    int [][] ordering = new int[this.numExamDays][2];
    Collections.shuffle(modules);

    for (int i = 0; i < ordering.length; i++) {

      int nextFirst = modules.get(i);                           // pick from start
      int nextSecond = modules.get(modules.size() - (1 + i));   // pick from end

      // Sort the 2 module numbers
      if (nextSecond < nextFirst) {
        int temp = nextFirst;
        nextFirst = nextSecond;
        nextSecond = temp;
      }

      int j = i - 1;
      // Insert into the next empty slot if the first module of the pick is greater
      // than the first module of the last record or if we are inserting the first element
      if (i == 0 || ordering[j][0] <= nextFirst) {
        ordering[j + 1][0] = nextFirst;
        ordering[j + 1][1] = nextSecond;
        continue;
      }

      // Starting from the end, slide up any element/record whos first module is greater
      // than the picked module
      for (; j >= 0 && ordering[j][0] > nextFirst; j--) {
        ordering[j + 1][0] = ordering[j][0];
        ordering[j + 1][1] = ordering[j][1];
      }

      // Insert the new picks in a sorted position
      ordering[j + 1][0] = nextFirst;
      ordering[j + 1][1] = nextSecond;
    }

    return ordering;
  }


  /*
   *  Checks if a sorted ordering is unique in O(P * D) time.
   *
   *  Sorting avoids an O(P * D^2) problem
   *  The only other way to avoid this would be a deep equals of both arrays
   *  but that would not account for cases where the order is just different
   *  when both orderings are actually not unique.
   *
   *  This catches both cases; either days are reordered, or the two exams are reordered
   */
  private boolean isUniqueSortedOrdering(int [][] ordering) {
    for (Ordering existingOrdering : this.currentGeneration) {
      boolean different = false;
      int[][] values = existingOrdering.getValues();
      for (int i = 0; i < values.length && !different; i++) {
        if (values[i][0] != ordering[i][0] || values[i][1] != ordering[i][1]) {
          // Since both are sorted, if any element differs at this index they must be different
          different = true;
          break;  // No need to go any further
        }
      }
      if (!different) {
        return false;   // Duplicate ordering found
      }
    }

    return true;
  }

  /*
   * Calculates the fitness cost of a given ordering.
   * Cost = number of conflicts/overlaps in the ordering (schedule) with the student schedules
   *
   * In:
   *  ordering (int[][]):   The ordering to calculate the fitness cost of
   *  studentSchedule (ArrayList<HashSet<Integer>>):  All the student schedules to find conflicts with
   *
   * Out:
   *  int:  The number of conflicts/collisions found
   *
   */
  private int calcFitnessCost(int [][] ordering) {
    int collisions = 0;

    for (HashSet<Integer> schedule : studentSchedule) {
      for (int [] pair : ordering) {  // For each pair of exams in the ordering
        // collision/conflict if the student must attend both exams
        if (schedule.contains(pair[0]) && schedule.contains(pair[1])) // O(1) look up in a set
          collisions++;
      }
    }

    return collisions;
  }

  /*
   * Generate P (population size) unique orderings and their cost.
   *
   * If it is getting difficult to create new unique orderings, 5 sequential failures will stop trying.
   *
   */
  private void generateCurrentGeneration() {
    for (int i = 0; i < this.populationSize; i++) {
      int tries = 5;  // Number of attempts to generate a new random ordering

      while (tries > 0) {
        int [][] values = generateSortedOrdering();
        if (isUniqueSortedOrdering(values)) {
          this.currentGeneration.add(new Ordering(values, calcFitnessCost(values)));
          break;
        }
        tries--;
        String popNum = String.format("[Population: %d]", i);
        if (tries == 0) {
          logger.warning(popNum + " Attempts to generate a unique ordering exceeded. Aborting further attempts.");
          return; // No point in trying for the remaining populations
        }
        logger.info("   " + popNum + " Failed to generate a unique ordering. Retrying " + tries + " more times.");
      }

    }
  }

  /*
   * The generated orderings were sorted for the purpose of efficiently checking for uniqueness.
   * This may interfere with our GA so they must be reshuffled.
   */
  private void shuffleCurrentGeneration() {
    for (Ordering ordering : this.currentGeneration) {
      int[][] values = ordering.getValues();
      Collections.shuffle(Arrays.asList(values));
      for (int[] day : values) {
        Collections.shuffle(Arrays.asList(day));
      }
      ordering.setValues(values);
    }

  }

  private void applySelection() {
    this.currentGeneration.sort(Comparator.comparing(Ordering::getFitness));

    int[] parts = findThreeClosestParts(this.currentGeneration.size());
    int part1_start = 0;
    int part3_start = parts[0] + parts[1];

    for (int i = 0; i < parts[0]; i++) {
      Ordering copy = new Ordering(this.currentGeneration.get(part1_start + i));
      this.currentGeneration.set(part3_start + i, copy);
      logger.info("Selection: Copying Ord" + (part1_start + i + 1) + " to Ord" + (part3_start + i + 1));
    }

  }

  /*
   * Given an integer (length of list) split it into the 3 closest possible parts.
   * The biggest part is always in the middle so that we can easily perform selection
   * on the top and bottom as they will be the same size.
   *
   * Examples:
   *    99  => [33,33,33]
   *    100 => [33,34,33]
   *    101 => [34,33,34] rather than [33,33,35]
   *    102 => [34,34,34]
   *    103 => [34,35,34]
   *
   */
  private static int[] findThreeClosestParts(int size) {
    Double original = size/3.0;
    int rounded = (int) Math.round(original);

    if(rounded < original) {
        // We rounded down
        return new int[]{ rounded, (int) Math.ceil(original), rounded };
    } else if (rounded > original) {
        // We rounded up
        return new int[]{ rounded, (int) Math.floor(original), rounded };
    } else {
        // No rounding
        return new int[]{ rounded, rounded, rounded };
    }
  }


  public ArrayList<HashSet<Integer>> getStudentSchedule() {
    return this.studentSchedule;
  }

  public ArrayList<Ordering> getCurrentGeneration() {
    return this.currentGeneration;
  }

}
